%%%-------------------------------------------------------------------
%%% @author Heinz N. Gies <heinz@licenser.net>
%%% @copyright (C) 2012, Heinz N. Gies
%%% @doc
%%%
%%% @end
%%% Created : 10 May 2012 by Heinz N. Gies <heinz@licenser.net>
%%%-------------------------------------------------------------------
-module(vmadm).
-behaviour(vmctl).

%% API
-export([start/1,
         start/2,
         stop/1,
         force_stop/1,
         info/1,
         reboot/1,
         force_reboot/1,
         delete/1,
         create/2,
         update/2,
         list/0,
         load/1
        ]).


%%%===================================================================
%%% API
%%%===================================================================

%%--------------------------------------------------------------------
%% @doc
%% @spec
%% @end
%%--------------------------------------------------------------------

list() ->
    Cmd = vmadm_cmd() ++ " list -pH",
    lager:debug("vmadm:cmd - ~s.", [Cmd]),
    L = os:cmd(Cmd),
    lager:debug("[vmadm] ~s", [L]),
    L1 = re:split(L, "\n"),
    L2 = [E || E <- L1, E =/= <<>>],
    L3 = [re:split(E, ":") || E <- L2],

    [#{uuid => UUID, name => UUID, state => VMState} ||
        [UUID, _Type, _Mem, VMState, _Alias] <- L3].

-spec start(UUID::fifo:uuid()) -> list().
start(UUID) ->
    cmd(UUID, "start").

-spec start(UUID::fifo:uuid(), Image::binary()) -> list().
start(UUID, _Image) ->
    cmd(UUID, "start").

-spec delete(UUID::fifo:uuid()) -> string().

delete(UUID) ->
    cmd(UUID, "delete").

-spec info(UUID::fifo:uuid()) -> {ok, fifo:config_list()} | {error, no_info}.

info(UUID) ->
    Output =  cmd(UUID, "info"),
    decode_info(Output).

-spec stop(UUID::fifo:uuid()) -> list().
stop(UUID) ->
    cmd(UUID, "stop").

-spec force_stop(UUID::fifo:uuid()) -> list().

force_stop(UUID) ->
    cmd(UUID, "stop -F").

-spec reboot(UUID::fifo:uuid()) -> list().

reboot(UUID) ->
    cmd(UUID, "reboot").

-spec force_reboot(UUID::fifo:uuid()) -> list().

force_reboot(UUID) ->
    cmd(UUID, "reboot -F").

-spec create(UUID::binary(), Data::fifo:vm_config()) -> ok |
                                                        {error, binary() |
                                                         timeout |
                                                         unknown}.
create(UUID, Data) ->
    lager:info("New Create: ~p", [Data]),
    lager:info("Creation of VM '~s' started.", [UUID]),
    lager:info("vmadm:create"),
    File = "/tmp/" ++ binary_to_list(UUID) ++ ".json",
    file:write_file(File, jsone:encode(Data)),
    Port = vmadm_port(["create", "-f", File]),
    lager:info("vmadm:create - handed to vmadm, waiting ...", []),
    Res = case wait_for_text(Port, UUID, 60*10) of
              ok ->
                  lager:info("vmadm:create - vmadm returned sucessfully", []),
                  chunter_vm_fsm:load(UUID);
              {error, E} ->
                  lager:error("vmad:create - Failed: ~p.", [E]),
                  delete(UUID),
                  {error, E}
          end,
    file:delete(File),
    lager:info("vmadm:create - updating memory.", []),
    chunter_server:update_mem(),
    ls_vm:creating(UUID, false),
    Res.

update(UUID, Data) ->
    lager:info("~p", [<<"Updating of VM '", UUID/binary, "' started.">>]),
    lager:info("vmadm:update", []),
    File = "/tmp/" ++ binary_to_list(UUID) ++ ".json",
    lager:debug("vmadm:cmd - vmadm update -f.", [File]),
    file:write_file(File, jsone:encode(Data)),
    Port = vmadm_port(["update", "-f", File, UUID]),
    Res = wait_for_result(Port, UUID),
    file:delete(File),
    Res.

load(UUID) when is_binary(UUID)  ->
    load(#{name => UUID, uuid => UUID});

load(#{uuid := UUID}) ->
    case fifo_cmd:run_json(vmadm_cmd(), ["get", UUID]) of
        {error, _, E} ->
            lager:warning("[~s] Could not read VM data: ~s", [UUID, E]),
            {error, not_found};
        R ->
            R
    end.

%%%===================================================================
%%% Internal functions
%%%===================================================================

cmd(UUID, Arg) ->
    lager:info("vmadm:~s - UUID: ~s.", [Arg, UUID]),
    Cmd = vmadm_cmd() ++ " " ++ Arg ++ " " ++ binary_to_list(UUID),
    lager:debug("vmadm:cmd - ~s.", [Cmd]),
    R = os:cmd(Cmd),
    lager:debug("[vmadm] ~s", [R]),
    R.

vmadm_cmd() ->
    case chunter_utils:system() of
        freebsd ->
            "/usr/local/sbin/vmadm";
        smartos ->
            "/usr/sbin/vmadm"
    end.

vmadm_port(Args) ->
    Cmd = vmadm_cmd(),
    lager:debug("vmadm:cmd - ~s ~p.", [Cmd, Args]),
    open_port({spawn_executable, Cmd},
              [{args, Args},
               use_stdio, binary, {line, 2048},
               stderr_to_stdout, exit_status]).

decode_info("Unable" ++ _) ->
    {error, no_info};

decode_info(JSON) ->
    try jsone:decode(list_to_binary(JSON)) of
        R ->
            {ok, R}
    catch
        _:_ ->
            {error, no_info}
    end.

wait_for_result(Port, UUID) ->
    receive
        {Port, {data, {eol, Data}}} ->
            lager:debug("[vmadm] ~s", [Data]),
            %% take good care of the `exit_status` message
            wait_for_result(Port, UUID);
        {Port, {data, Data}} ->
            lager:debug("[vmadm] ~s", [Data]),
            wait_for_result(Port, UUID);
        {Port, {exit_status, 0}} ->
            chunter_server:update_mem(),
            chunter_vm_fsm:load(UUID);
        {Port, {exit_status, E}} ->
            chunter_server:update_mem(),
            chunter_vm_fsm:load(UUID),
            {error, E}
    after
        60000 ->
            chunter_server:update_mem(),
            chunter_vm_fsm:load(UUID)
    end.

wait_for_text(Port, Lock, Max) ->
    wait_for_text(Port, Lock, Max, erlang:system_time(seconds)).

wait_for_text(Port, Lock, Max, T0) ->
    receive
        {Port, {data, {eol, Data}}} ->
            lager:debug("[vmadm] ~s", [Data]),
            relock(Lock),
            wait_for_text(Port, Lock, Max, T0);
        {Port, {data, Data}} ->
            lager:debug("[vmadm] ~s", [Data]),
            relock(Lock),
            wait_for_text(Port, Lock, Max, T0);
        {Port, {exit_status, 0}} ->
            ok;
        {Port, {exit_status, S}} ->
            {error, S}
    after
        3000 ->
            case erlang:system_time(seconds) - T0 of
                _To when _To  > Max ->
                    lager:debug("[vmadm] timeout after ~ps", [Max]);
                _ ->
                    relock(Lock),
                    wait_for_text(Port, Lock, Max, T0)
            end
    end.
relock(Lock) ->
    chunter_lock:lock(Lock).
